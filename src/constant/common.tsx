export interface BlogCardProps {
    id?: string;
    image: string;
    avatar: string;
    title: string;
    user: string;
    time:string;
  }

  
export interface CourseCardProps {
  link:string,
    id?: string;
    image: string;
    members: string;
    title: string;
  }

export  interface VideoCardProps {
    id?: string;
    image: string;
    view: string;
    title: string;
    like: string;
    mess: string;
  }
  
  export interface HeaderrProps{
    logo:string,
    slogan:string,
    inputText:string,
    loginBtn:string
  }
  
  export interface LandingHeaderProps {
    logo: string;
    slogan: string;
    menu: string[];
    textColor?:string
  }

export interface LandingSectionProps{
    title: string,
    content: string,
    btnText:string,
    btnText_ps?:string,
    content_ps?:string,
}

export interface LandingContentProps {
  landingMenu: Array<Array<string>>;
}

export interface LandingCardProps {
  content: string;
  avatar: string;
  user: string;
  course: string;
}

export interface LandingChatProps {
  rotate: boolean;
  setRotate: (chatOpen: boolean) => void;

  chatOpen: boolean;
  setChatOpen: (chatOpen: boolean) => void;
}

interface Lesson {
  id: string;
  title: string;
  content: LessonContent[];
  amount: string;
}

interface LessonContent {
  name: string;
  time: string;
}

export interface CourseDetailProps {
  lessons: Lesson[];
}