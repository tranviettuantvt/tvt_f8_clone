import { Meta, Story } from "@storybook/react";
import { BrowserRouter } from "react-router-dom";
import CourseCollapse from "./CourseCollapse";
import { CourseDetailProps} from "../../../constant/common";

const meta: Meta = {
  title: "Organisms/CourseCollapse",
  component: CourseCollapse,
  tags: ["autodocs"],
};

export default meta;

const Template: Story<CourseDetailProps> = (args) => (
  <BrowserRouter>
    <CourseCollapse {...args} />
  </BrowserRouter>
);

export const Default = Template.bind({});
Default.args={
    lessons:[
        {
            id:"1",
            title: "Khái niệm cần biêt",
            content: [
              { name: "1. Mô hình Clien - Server là gì", "time": "11:35" },
              { name: "2. Domain là gì? Tên miền là gì?", "time": "10:34" }
            ],
            amount:"2 bài học"
          },
          {
            id:"2",
            title: "Môi trường, con người IT",
            content: [
              { name: "3. Học IT cần tố chất gì? Góc nhìn khác từ chuyên gia định hướng giáo dục", "time": "11:35" },
              { name: "4. Sinh viên IT đi thực tập tại doanh nghiệp cần biết những gì?", "time": "10:34" },
              { name: "5. Trải nghiệm thực tế sau 2 tháng làm việc tại doanh nghiệp của học viên F8?", "time": "10:34" }
        
            ],
            amount:"3 bài học"
          },
          {
            id:"3",
            title: "Phương pháp định hướng",
            content: [
              { name: "6. Phương pháp học lập trình của Admin F8?", "time": "11:35" },
              { name: "7. Làm sao để có thu nhập cao và đi xa hơn trong ngành IT? ", "time": "10:34" },
              { name: "8. 8 lời khuyên giúp học lập trình tại F8 hiệu quả hơn!", "time": "10:34" },
              { name: "9. Tại sao nên học trên website này hơn là học trên Youtube?", "time": "10:34" }
            ],
            amount:"4 bài học"
          }
    ]
}