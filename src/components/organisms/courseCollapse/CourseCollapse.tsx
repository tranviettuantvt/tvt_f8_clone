import { Collapse, CollapseProps } from "antd";
import React, { memo } from "react";
import { CourseDetailProps } from "../../../constant/common";
import "./CourseCollapse.scss";
import { ReactComponent as YourSvg } from "../../../assets/youtubesvg.svg";
import {ReactComponent as NoteSvg} from '../../../assets/notesvg.svg'
import { MinusOutlined, PlusOutlined } from "@ant-design/icons";
import type { CSSProperties } from "react";

const CourseCollapse: React.FC<CourseDetailProps> = ({ lessons }) => {
  const getItems = (panelStyle: CSSProperties): CollapseProps["items"] =>
    lessons.map((lesson) => ({
      key: lesson.id,
      label: (
        <div className="collapse__label">
          <span>{lesson.title}</span>
          <span>{lesson.amount}</span>
        </div>
      ),
      children: (
        <div className="collapse__content">
          {lesson.content.map((lessonContent, index) => (
            <div className="collapse__content__item" key={index}>
              <div>
                <YourSvg />
                <span>{lessonContent.name}</span>
              </div>
              <span>{lessonContent.time}</span>
            </div>
          ))}
        </div>
      ),
      style: panelStyle,
    }));

  const items: CollapseProps["items"] = [
    {
      key: "1",
      label: (
        <div className="collapse__label">
          <span>Hoàn thành khóa học</span>
          <span>2 bài học</span>
        </div>
      ),
      children: (
        <div className="collapse__content">
          <div className="collapse__content__item">
            <div>
              <NoteSvg />
              <span>10. Ứng tuyển xin việc làm</span>
            </div>
            <span>12:00</span>
          </div>
          <div className="collapse__content__item">
            <div>
              <NoteSvg />
              <span>11. Hoàn thành khóa học</span>
            </div>
            <span>01:00</span>
          </div>
        </div>
      ),
    },
  ];
  const panelStyle = {
    marginBottom: 16,
  };

  return (
    <div id="courseCollapse">
      <h3>Nội dung khóa học</h3>
      <div className="courseCollapse__infor">
        <ul>
          <li>
            <strong>4</strong> chương
          </li>
          <li>
            <strong>11</strong> bài học
          </li>

          <li>
            Thời lượng <strong>03 giờ 25 phút</strong>
          </li>
        </ul>
        <a>Mở rộng tất cả</a>
      </div>
      <Collapse
        items={getItems(panelStyle)}
        defaultActiveKey={["1"]}
        expandIcon={({ isActive }) =>
          isActive ? <MinusOutlined /> : <PlusOutlined />
        }
      />
      <Collapse
        items={items}
        defaultActiveKey={["1"]}
        expandIcon={({ isActive }) =>
          isActive ? <MinusOutlined /> : <PlusOutlined />
        }
      />
    </div>
  );
};

export default memo(CourseCollapse);
