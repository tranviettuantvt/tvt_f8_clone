import { Meta, Story } from "@storybook/react";
import { BrowserRouter } from "react-router-dom";
import LandingChat from "./LandingChat";
import { LandingChatProps, LandingContentProps } from "../../../constant/common";

const meta: Meta = {
  title: "Organisms/LandingChat",
  component: LandingChat,
  tags: ["autodocs"],
};

export default meta;

const Template: Story<LandingChatProps> = (args) => (
  <BrowserRouter>
    <LandingChat {...args} />
  </BrowserRouter>
);
export const Default=Template.bind({})
Default.args={
  chatOpen:true,
  rotate:true,
  setChatOpen:()=> {},
  setRotate:()=> {}

}