import React, { memo } from "react";
import "./LandingChat.scss";
import LoginBtn from "../../atoms/loginbtn/LoginBtn";
import Chatsvg from "../../../assets/Chatsvg.svg";
import {
  ArrowRightOutlined,
  CloseOutlined,
  MessageOutlined,
} from "@ant-design/icons";
import { LandingChatProps } from "../../../constant/common";


const LandingChat: React.FC<LandingChatProps> = ({ chatOpen, setChatOpen ,rotate, setRotate}) => {
  const handleClick =() => {
    setChatOpen(false)
    setRotate(false)
  }
    return (
    <>
      {chatOpen && (
        <div id="landingChat">
          <div className="landingChat">
            <div className="landingChat__header">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <img
                    src="https://w.ladicdn.com/s200x200/63e927420fe32f0012236125/chat-avatar-landing-page-20230212181748-mpnc6.png"
                    alt=""
                  />
                  <span>Xin chào 👋</span>
                </div>
                <CloseOutlined onClick={handleClick} />
              </div>
              <p>Mình là Oanhh, rất vui được hỗ trợ bạn ^^</p>
            </div>
            <div className="landingChat__body">
              <div className="body__first">
                <p>Chat với chúng tôi</p>
                <span>Hỗ trợ viên đang trực tuyến sẵn sàng hỗ trợ</span>
                <div className="first__mess">
                  <img
                    className="avatar"
                    src="https://w.ladicdn.com/s200x200/63e927420fe32f0012236125/chat-avatar-landing-page-20230212181748-mpnc6.png"
                    alt=""
                  />
                  <LoginBtn imglink={Chatsvg} text="Bắt đầu chát" />
                </div>
              </div>
              <div className="body__second">
                <p>Hoặc liên hệ qua:</p>
                <div className="second__mess">
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      gap: "0.5rem",
                    }}
                  >
                    {" "}
                    <img
                      src="https://w.ladicdn.com/ladiui/icons/ldicon-ladichat-messenger.svg"
                      alt=""
                    />{" "}
                    <span>Messenger</span>
                  </div>
                  <ArrowRightOutlined />
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default memo(LandingChat);
