import {
  AppstoreOutlined,
  BoxPlotOutlined,
  CheckOutlined,
  ClockCircleOutlined,
  IssuesCloseOutlined,
} from "@ant-design/icons";
import { Col, Row } from "antd";
import React, { memo } from "react";
import "./CourseDetail.scss";
import CourseCollapse from "../../organisms/courseCollapse/CourseCollapse";
import Buttonn from "../../atoms/button/Buttonn";
const lesson = [
  {
    id: "1",
    title: "Khái niệm cần biêt",
    content: [
      { name: "1. Mô hình Clien - Server là gì", time: "11:35" },
      { name: "2. Domain là gì? Tên miền là gì?", time: "10:34" },
    ],
    amount: "2 bài học",
  },
  {
    id: "2",
    title: "Môi trường, con người IT",
    content: [
      {
        name: "3. Học IT cần tố chất gì? Góc nhìn khác từ chuyên gia định hướng giáo dục",
        time: "11:35",
      },
      {
        name: "4. Sinh viên IT đi thực tập tại doanh nghiệp cần biết những gì?",
        time: "10:34",
      },
      {
        name: "5. Trải nghiệm thực tế sau 2 tháng làm việc tại doanh nghiệp của học viên F8?",
        time: "10:34",
      },
    ],
    amount: "3 bài học",
  },
  {
    id: "3",
    title: "Phương pháp định hướng",
    content: [
      { name: "6. Phương pháp học lập trình của Admin F8?", time: "11:35" },
      {
        name: "7. Làm sao để có thu nhập cao và đi xa hơn trong ngành IT? ",
        time: "10:34",
      },
      {
        name: "8. 8 lời khuyên giúp học lập trình tại F8 hiệu quả hơn!",
        time: "10:34",
      },
      {
        name: "9. Tại sao nên học trên website này hơn là học trên Youtube?",
        time: "10:34",
      },
    ],
    amount: "4 bài học",
  },
];
const CourseDetail: React.FC = () => {
  return (
    <div id="courseDetail">
      <Col lg={12} className="first__header">
        <h1>Kiến Thức Nhập Môn IT</h1>
        <p>
          Để có cái nhìn tổng quan về ngành IT - Lập trình web các bạn nên xem
          các videos tại khóa này trước nhé.
        </p>
      </Col>
      <Row className="courseDetail" gutter={[24, 24]} justify={"space-between"}>
        <Col lg={16} md={24} className="detail__first">
          <div className="first__props">
            <h2>Bạn sẽ học được gì?</h2>
            <Row gutter={[24, 20]}>
              <Col lg={12} md={12} xs={24} sm={24}>
                <CheckOutlined /> Các kiến thức cơ bản, nền móng của ngành IT
              </Col>
              <Col lg={12} md={12} xs={24} sm={24}>
                <CheckOutlined /> Các mô hình, kiến trúc cơ bản khi triển khai
                ứng dụng
              </Col>
              <Col lg={12} md={12} xs={24} sm={24}>
                <CheckOutlined /> Các khái niệm, thuật ngữ cốt lõi khi triển
                khai ứng dụng
              </Col>
              <Col lg={12} md={12} xs={24} sm={24}>
                <CheckOutlined /> Hiểu hơn về cách internet và máy vi tính hoạt
                động
              </Col>
            </Row>
          </div>
          <CourseCollapse lessons={lesson} />
        </Col>
        <Col lg={7} md={24} className="detail__second">
          <div className="second__img">
            <img
              src="https://files.fullstack.edu.vn/f8-prod/courses/7.png"
              alt=""
            />
            <p>Xem giới thiệu khóa học</p>
          </div>
          <div className="second__body">
            <p>Miễn Phí</p>
            <Buttonn
              size="large"
              className="course__btn"
              backgroundColor="#f05123"
            >
              ĐĂNG KÝ HỌC
            </Buttonn>

            <ul className="second__body__list">
              <li>
                <IssuesCloseOutlined /> <span>Trình độ cơ bản</span>
              </li>
              <li>
                <AppstoreOutlined />{" "}
                <span>
                  Tổng số <strong>11</strong> bài học
                </span>
              </li>
              <li>
                <ClockCircleOutlined />{" "}
                <span>
                  Thời lượng <strong>03 giờ 25 phút</strong>
                </span>
              </li>
              <li>
                <BoxPlotOutlined /> <span>Học mọi lúc, mọi nơi</span>
              </li>
            </ul>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default memo(CourseDetail);
